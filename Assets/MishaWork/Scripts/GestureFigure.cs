﻿using System;
using UnityEngine;

namespace AnimalBattle
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class GestureFigure : MonoBehaviour
    {
        [HideInInspector]
        public SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        //animation event function
        public void DestroyFigure()
        {
            Destroy(this.gameObject);
        }
    }
}