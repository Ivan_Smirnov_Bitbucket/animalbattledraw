﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalBattle
{
    public class GesturesSpawner : MonoBehaviour
    {
        [SerializeField] private Arrow gesturePrefab;
        [SerializeField] private RectTransform spawnPointTransform;
        [SerializeField] private RectTransform targetTransform;
        [SerializeField] private float paddingFromTarget;
        public float gestureSpeed = 140;
        public float delayBetweenSpawns = 1.0f;

        private SpriteWithGesture[] _gesturesToSpawn;

        private float _delayBetweenSpawns;
        private int _currentGestureIndex;
        private bool _isSpawning;
        
        private RectTransform _lastSpawnedGestureTransform;
        public List<Arrow> gesturesList;

        private void Awake()
        {
            Vector2 targetPosition = targetTransform.position;
            gesturesList = new List<Arrow>();
            
            spawnPointTransform.position = new Vector2(targetPosition.x + paddingFromTarget,
                targetPosition.y);
        }

        public void StartSpawningGestures(SpriteWithGesture[] gestures)
        {
            _gesturesToSpawn = gestures;
            _isSpawning = true;
        }

        private void Update()
        {
            if (_isSpawning)
            {
                if (_currentGestureIndex >= _gesturesToSpawn.Length) _isSpawning = false;
                
                _delayBetweenSpawns -= Time.deltaTime;
                if (_delayBetweenSpawns <= 0)
                {
                    SpawnGesture();
                    _delayBetweenSpawns = delayBetweenSpawns;
                }
            }
            
        }

        private void SpawnGesture()
        {
            var instantiatedArrow = Instantiate(gesturePrefab, spawnPointTransform.transform);
            gesturesList.Add(instantiatedArrow);
            instantiatedArrow.leftSpeed = gestureSpeed;

            instantiatedArrow.gesturePattern = _gesturesToSpawn[_currentGestureIndex].gesturePattern;
            instantiatedArrow.gestureImage.sprite = _gesturesToSpawn[_currentGestureIndex].gestureSprite;
            instantiatedArrow.isLast = (_currentGestureIndex == _gesturesToSpawn.Length - 1);

            _currentGestureIndex++;
        }

        public void ClearTheList()
        {
            _isSpawning = false;
            
            foreach (var gesture in gesturesList)
            {
                Destroy(gesture.gameObject);
            }
            
            gesturesList.Clear();
        }

        public void ReloadSpawnerToNewWave(SpriteWithGesture[] patterns)
        {
            _currentGestureIndex = 0;
            
            ClearTheList();
            
            _gesturesToSpawn = patterns;
            StartSpawningGestures(_gesturesToSpawn);
        }
    }
}