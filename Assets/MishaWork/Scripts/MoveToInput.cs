﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToInput : MonoBehaviour
{
    [SerializeField] private RectTransform target;
    void Update()
    {
        Vector2 targetWorldPosition = Camera.main.ScreenToWorldPoint(target.position);
        transform.position = new Vector3(targetWorldPosition.x, 2, targetWorldPosition.y);
    }
}
