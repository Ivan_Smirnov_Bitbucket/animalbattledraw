﻿
using GestureRecognizer;
using UnityEngine;

[CreateAssetMenu(fileName = "SpriteWithGesture", menuName = "Create Sprite With Gesture", order = 3)]
public class SpriteWithGesture : ScriptableObject
{
        public GesturePattern gesturePattern; 
        public Sprite gestureSprite;
}
