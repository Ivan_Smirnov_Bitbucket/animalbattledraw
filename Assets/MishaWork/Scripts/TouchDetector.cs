﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchDetector : MonoBehaviour
{
    private Rect _touchZone;
    private void Awake()
    {
        _touchZone = new Rect(0, Screen.height / 5, Screen.width, Screen.height - Screen.height / 2);
    }
    

    public bool TouchInZone()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            var touchPoint = touch.position;
            
            if (_touchZone.Contains(touchPoint))
            {
                return true;
            }
        }
        
        return false;
    }
}
