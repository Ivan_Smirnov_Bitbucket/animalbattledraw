﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AnimalBattle
{
    [RequireComponent(typeof(Health))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Rotator))]
    public abstract class Entity : MonoBehaviour
    {
        [SerializeField] private int minDamage;
        [SerializeField] private int maxDamage;
        [SerializeField] private TrailRenderer[] attackTrails;

        //private float damage;
        public float damage;

        public bool damageIsSet = false;

        public CharacterData characterData;
        
        [HideInInspector]
        public Animator animator;
        [HideInInspector]
        public Health health;

        public Rotator rotator;
        private Entity enemyEntity;

        protected int _lastPlayedAnimationIndex;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            health = GetComponent<Health>();
            rotator = GetComponent<Rotator>();
            
            animator.SetBool(AnimationHelper.IS_DEFENDING_BOOL, true);
        }

        public void TurnOnTrails()
        {
            foreach (var trail in attackTrails)
            {
                trail.Clear();
                
                trail.emitting = true;
            }
        }

        public void TurnOffTrails()
        {
            foreach (var trail in attackTrails)
            {
                trail.emitting = false;
            }
        }
        

        public void StartAttack()
        {
            animator.SetBool(AnimationHelper.IS_DEFENDING_BOOL, false);
            TurnOnTrails();
            PlayRandomAttackAnimation();
        }
        
        public void Resurrect()
        {
            animator.SetBool(AnimationHelper.IS_DEFENDING_BOOL, true);
            animator.SetTrigger(AnimationHelper.RESURRECT_TRIGGER);
            animator.ResetTrigger(AnimationHelper.RESURRECT_TRIGGER);
            rotator.RotateToStartPosition();
            health.isAlive = true;
            health.healthPoints = 100;
        }

        protected void PlayRandomAttackAnimation()
        {
            int animationIndex = Random.Range(1, 6);
            animator.SetTrigger(AnimationHelper.ATTACK_TRIGGER + animationIndex.ToString());
        }

        protected int GetIndexOfNextAnimation()
        {
            _lastPlayedAnimationIndex++;
            if (_lastPlayedAnimationIndex > 5) _lastPlayedAnimationIndex = 1;

            return _lastPlayedAnimationIndex;
        }

        public virtual void Attack() { }

        protected int CountDamage()
        {
            return Random.Range(minDamage, maxDamage + 1);
        }
        
    }
}