﻿using System;
using UnityEngine;

namespace AnimalBattle
{
    public class ObjectsDatabase : MonoBehaviour
    {
        public static ObjectsDatabase instance;

        private void Awake()
        {
            if (instance == null) instance = this;
            else Destroy(gameObject);
        }

        public Transform textPopupPrefab;
    }
}