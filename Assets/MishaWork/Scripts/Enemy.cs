﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AnimalBattle
{
    public class Enemy : Entity
    {
        private PlayerCharacter _playerCharacter;
        
        private void Start()
        {
            _playerCharacter = (PlayerCharacter)GameController.instance.playerCharacter;
            health.onDieEvent += Health_OnDieEvent;
        }

        public void Health_OnDieEvent(object sender, EventArgs e)
        {
            rotator.Rotate(0, 10);
            animator.SetTrigger(AnimationHelper.DIE_TRIGGER);
            GameController.instance.playerCharacter.animator.SetTrigger(AnimationHelper.VICTORY_TRIGGER);
            GameController.instance.playerCharacter.rotator.Rotate(20, 20);
            GameController.instance.mainCamera.GetComponent<Animator>().
                SetBool(AnimationHelper.IS_PLAYER_ZOOMING_BOOL, true);
            GameController.instance.confettyParticles.Play();
            GameController.instance.CurrentState = GameState.SceneReloading;
            GameController.instance.NextLevel();
            Debug.Log("Player is win");
        }

        private IEnumerator AttackWithDelay(Entity enemyEntity)
        {
            yield return new WaitForSeconds(0.5f);
            animator.SetTrigger("attack");
            enemyEntity.health.ApplyDamage(CountDamage());
            yield return new WaitForSeconds(0.5f);
        }

        public override void Attack()
        {
            float damageToApply;
            damageToApply = damageIsSet ? damage : Random.Range(10, 21);
            _playerCharacter.health.ApplyDamage(damageToApply);
            animator.SetBool(AnimationHelper.IS_DEFENDING_BOOL, true);
        }
    }
}