﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GestureController : MonoBehaviour
{
    [SerializeField] private Camera eventCamera;

    private TrailRenderer _trailRenderer;
    private Vector2 _inputPosition;

    private void Awake()
    {
        _trailRenderer = GetComponent<TrailRenderer>();
    }

    void Update()
    {
        /*
        //standalone
        if (Input.GetMouseButtonDown(0))
        {
            _trailRenderer.emitting = true;
            _trailRenderer.Clear();
        }

        if (Input.GetMouseButtonUp(0))
        {
            _trailRenderer.emitting = false;
        }
        
        inputPosition = eventCamera.ScreenToWorldPoint(Input.mousePosition);
        
        */
        
        //touch detect
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                transform.position = TouchToWorldPoint(touch);
                _trailRenderer.Clear();
                _trailRenderer.emitting = true;
            }

            if (touch.phase == TouchPhase.Moved)
            {
                transform.position = TouchToWorldPoint(touch);
            }

            if (touch.phase == TouchPhase.Ended)
            {
                _trailRenderer.emitting = false;
            }
        }
        
    }
    
    private Vector2 TouchToWorldPoint(Touch touch)
    {
        return eventCamera.ScreenToWorldPoint(touch.position);
    }
}
