﻿using System;
using System.Collections;
using System.Collections.Generic;
using Exploder;
using Exploder.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace AnimalBattle
{
    public class Health : MonoBehaviour
    {
        public float healthPoints = 100;
        [SerializeField] private ExploderObject shield;

        public EventHandler onDieEvent;
        
        private HealthBarPresenter _healthBarPresenter;

        private bool _damageHealthBarIsMoving;
        private bool _wasShieldDestroy;
        private float _damageHealthBarChangingAmount;

        private Entity _thisEntity;
        private float _maxHealthPoints;
        public bool isAlive = true;


        private void Start()
        {
            isAlive = true;
            _maxHealthPoints = healthPoints;
            _thisEntity = GetComponent<Entity>();
            
            if (_thisEntity is PlayerCharacter)
            {
                _healthBarPresenter = GameController.instance.playerCharacterHealthBarPresenter;
            }
            else
            {
                _healthBarPresenter = GameController.instance.enemyHealthBarPresenter;
            }
        }

        private void Update()
        {
            if (!_damageHealthBarIsMoving) return;

            _healthBarPresenter.damageBar.fillAmount -= _damageHealthBarChangingAmount;
            if (_healthBarPresenter.damageBar.fillAmount <= _healthBarPresenter.currentHealthBar.fillAmount)
            {
                _damageHealthBarIsMoving = false;
            }

        }

        public void ApplyDamage(float damageAmount)
        {
            if (!isAlive) return;

            Vector3 position = transform.position;
            Vector3 popupSpawnPosition = new Vector3(position.x, position.y + 2,
                position.z);
            TextPopup.Create(popupSpawnPosition, 1.5f, damageAmount.ToString());
            if (damageAmount < 0) damageAmount = 0;

            //yield return new WaitForSeconds(0.4f);
            
            if (healthPoints - damageAmount > 0)
                CheckForDamageAndPlayAnimation(damageAmount);
            
            healthPoints -= damageAmount;
            if (healthPoints <= 0)
            {
                GameController.instance.FinishTheSession();
                Die();
                healthPoints = 0;
            }

            //yield return new WaitForSeconds(0.2f);

            if (healthPoints < (_maxHealthPoints * 0.5))
            {
                //Break tha shield
                if (!_wasShieldDestroy)
                {
                    shield.ExplodeObject(shield.gameObject);
                    _wasShieldDestroy = true;
                }
            }

            UpdateHealthBar(damageAmount);
        }

        private void CheckForDamageAndPlayAnimation(float damageAmount)
        {
            if (!(damageAmount < healthPoints)) return;

            if (damageAmount > 0 && damageAmount <= 45)
            {
                _thisEntity.animator.SetTrigger(
                    AnimationHelper.SMALL_DAMAGE_TRIGGER);
            }
            else if (damageAmount > 45)
            {
                _thisEntity.animator.SetTrigger(
                    AnimationHelper.BIG_DAMAGE_TRIGGER);
            }
        }

        private void UpdateHealthBar(float damageAmount)
        {
            _healthBarPresenter.currentHealthBar.fillAmount -= damageAmount / _maxHealthPoints;
            _healthBarPresenter.currentHealthText.text = healthPoints.ToString();
            StartDamageHealthBarAnimation(damageAmount);
        }

        private void StartDamageHealthBarAnimation(float damageAmount)
        {
            _damageHealthBarIsMoving = true;
            _damageHealthBarChangingAmount = Time.deltaTime * (damageAmount / _maxHealthPoints);
        }

        private void Die()
        {
            isAlive = false;
            onDieEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}
