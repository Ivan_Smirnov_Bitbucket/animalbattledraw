﻿using UnityEngine;
using UnityEngine.UI;

namespace AnimalBattle
{
    public class HealthBarPresenter : MonoBehaviour
    {
        public Image currentHealthBar;
        public Image damageBar;
        public Text currentHealthText;
        public Image characterIcon;
    }
}