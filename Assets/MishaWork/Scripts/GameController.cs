﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using AnimalBattle;
using GestureRecognizer;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public enum GameState
{
    Playing, SceneReloading
}

public class GameController : MonoBehaviour
{
    public static GameController instance;

    [SerializeField] private int currentLevel = 1;
    [SerializeField] private Text levelText;

    [SerializeField] private PlayerCharacter[] characters;
    [SerializeField] private Target target;

    [Header("Gestures Information")] 
    [SerializeField] private int maxGestures = 5;
    [SerializeField] private GestureController gestureParticlesTrail;
    [SerializeField] private GesturesSpawner gestureSpawner;
    [SerializeField] private SpriteWithGesture[] easyGestures;
    [SerializeField] private SpriteWithGesture[] normalGestures;
    [SerializeField] private SpriteWithGesture[] hardGestures;
    [Space]
    [SerializeField] private Enemy[] enemies;
    [SerializeField] private Image gestureImage;
    [SerializeField] private TextMeshProUGUI gestureCompleteAccuracyText;
    [SerializeField] private Recognizer gestureRecognizer;
    [Space]
    public GameState CurrentState = GameState.Playing;
    public HealthBarPresenter playerCharacterHealthBarPresenter;
    public HealthBarPresenter enemyHealthBarPresenter;
    public ParticleSystem figureWinParticles;
    public ParticleSystem confettyParticles;
    public Camera mainCamera;
    
    public PlayerCharacter playerCharacter;
    public Enemy enemyCharacter;

    [SerializeField] private bool isPlayerTurn;

    private SpriteWithGesture[] _patterns;

    public bool isBattleSessionActive = true;

    private int currentCharactersIndex = 0;
    private ParticleSystem _trailParticleSystem;

    private void Awake()
    {
        _trailParticleSystem = gestureParticlesTrail.GetComponent<ParticleSystem>();
        _trailParticleSystem.Stop();
        
        if (instance == null)
        {
            instance = this;
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        FormingTheGesturesArray();
        playerCharacter.TurnOffTrails();
        enemyCharacter.TurnOffTrails();

        //Move();
    }

    public void EndGame()
    {
        CurrentState = GameState.SceneReloading;
        
        gestureSpawner.ClearTheList();
    }

    private IEnumerator ReloadScene()
    {
        playerCharacter.comboPoints = 0;
        playerCharacter.damageIsSet = false;
        enemyCharacter.damageIsSet = false;

        FormingTheGesturesArray();
        gestureSpawner.ReloadSpawnerToNewWave(_patterns);
        TurnOffParticles();

        yield return new WaitForSeconds(3.0f);
        
        levelText.text = "LEVEL " + currentLevel.ToString();
        target.gameObject.SetActive(true);
        
        mainCamera.GetComponent<Animator>().SetBool(AnimationHelper.IS_PLAYER_ZOOMING_BOOL, false);
        mainCamera.GetComponent<Animator>().SetBool(AnimationHelper.IS_ENEMY_ZOOMING_BOOL, false);
        confettyParticles.Stop();
        
        ChangeHeroes();
        
        playerCharacter.TurnOffTrails();
        enemyCharacter.TurnOffTrails();
    }

    public void TurnOffParticles()
    {
        figureWinParticles.Stop();
    }

    public void NextLevel()
    {
        currentLevel++;
        target.gameObject.SetActive(false);
        gestureSpawner.gestureSpeed = gestureSpawner.gestureSpeed +
                                      gestureSpawner.gestureSpeed * 0.15f;
        gestureSpawner.delayBetweenSpawns += gestureSpawner.delayBetweenSpawns * 0.02f;
        
        StartCoroutine(ReloadScene());
        CurrentState = GameState.Playing;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _trailParticleSystem.Play();
        }

        if (Input.GetMouseButtonUp(0))
        {
            _trailParticleSystem.Stop();
        }
    }


    private void FormingTheGesturesArray()
    {
        _patterns = new SpriteWithGesture[maxGestures];
        if (currentLevel <= 2)
        {
            for (int i = 0; i < _patterns.Length; ++i)
            {
                _patterns[i] = easyGestures[Random.Range(0, easyGestures.Length - 1)];
            }
        }
        else if (currentLevel > 2 && currentLevel <= 4)
        {
            for (int i = 0; i < _patterns.Length; ++i)
            {
                _patterns[i] = normalGestures[Random.Range(0, normalGestures.Length - 1)];
            }
        }
        else
        {
            for (int i = 0; i < _patterns.Length; ++i)
            {
                _patterns[i] = hardGestures[Random.Range(0, hardGestures.Length - 1)];
            }
        }

        gestureSpawner.StartSpawningGestures(_patterns);
    }

    public void PlayerAttack(int scoreDamage)
    {
        int damageAmount;
        
        if (scoreDamage > 70 && scoreDamage <= 80)
        {
            damageAmount = Random.Range(11, 21);
        }
        else if (scoreDamage > 80 && scoreDamage <= 90)
        {
            damageAmount = Random.Range(21, 31);
        }
        else if (scoreDamage > 90)
        {
            damageAmount = Random.Range(31, 41);
        }
        else damageAmount = 0;

        if (playerCharacter.damageIsSet == false)
            playerCharacter.damage = damageAmount;
        playerCharacter.StartAttack();
        
    }

    public void EnemyAttack()
    {
        enemyCharacter.StartAttack();
    }

    public void ChangeHeroes()
    {
        currentCharactersIndex++;

        if (currentCharactersIndex >= characters.Length) currentCharactersIndex = 0;
        
        for (int i = 0; i < characters.Length; ++i)
        {
            if (i != currentCharactersIndex)
            {
                characters[i].gameObject.SetActive(false);
                enemies[i].gameObject.SetActive(false);
            }
            else
            {
                characters[i].gameObject.SetActive(true);

                enemies[i].gameObject.SetActive(true);

                UpdateHealthBarsPresenters(characters[i].characterData,
                    enemies[i].characterData);

                playerCharacter = characters[i];
                if (!playerCharacter.health.isAlive) playerCharacter.Resurrect();
                enemyCharacter = enemies[i];
                if (!enemyCharacter.health.isAlive) enemyCharacter.Resurrect();
                
                playerCharacter.animator.SetBool(AnimationHelper.IS_DEFENDING_BOOL, true);
                playerCharacter.health.isAlive = true;
                playerCharacter.health.healthPoints = 100;
                playerCharacter.rotator.RotateToStartPosition();

                enemyCharacter.animator.SetBool(AnimationHelper.IS_DEFENDING_BOOL, true);
                enemyCharacter.health.isAlive = true;
                enemyCharacter.health.healthPoints = 100;
                enemyCharacter.rotator.RotateToStartPosition();
            }
        }

        isBattleSessionActive = true;
    }

    private void UpdateHealthBarsPresenters(CharacterData playerData, CharacterData enemyData)
    {
        playerCharacterHealthBarPresenter.damageBar.fillAmount = 1;
        playerCharacterHealthBarPresenter.currentHealthBar.fillAmount = 1;
        //в дальнейшем изменится на максимальное количество ХП персонажа
        playerCharacterHealthBarPresenter.currentHealthText.text = 100.ToString();
        playerCharacterHealthBarPresenter.characterIcon.sprite = playerData.characterIcon;
        
        enemyHealthBarPresenter.damageBar.fillAmount = 1;
        enemyHealthBarPresenter.currentHealthBar.fillAmount = 1;
        //в дальнейшем изменится на максимальное количество ХП персонажа
        enemyHealthBarPresenter.currentHealthText.text = 100.ToString();
        enemyHealthBarPresenter.characterIcon.sprite = enemyData.characterIcon;
    }

    public void FinishTheSession()
    {
        isBattleSessionActive = false;
    }

    public void WarnAboutRecognize(RecognitionResult recognitionResult)
    {
        if (recognitionResult == RecognitionResult.Empty) return;
        
        int points = Mathf.RoundToInt(recognitionResult.score.score * 100);
        Debug.Log("Recognize! " + points);
        gestureCompleteAccuracyText.text = points.ToString() + "%";
    }
    
}
