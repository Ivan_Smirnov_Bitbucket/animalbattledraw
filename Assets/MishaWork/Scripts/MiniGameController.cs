﻿using System;
using System.Collections;
using System.Collections.Generic;
using AnimalBattle;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class MiniGameController : MonoBehaviour, IPointerDownHandler
{
    [Header("Circle target range")]
    [SerializeField] private Vector3 minTargetPoint;
    [SerializeField] private Vector3 maxTargetPoint;
    [Space]
    [Header("GameObjects references")]
    [SerializeField] private Transform bigTargetTransform;
    [SerializeField] private Transform smallTargetTransform;
    [SerializeField] private TextMeshPro damageText;
    [SerializeField] private MoveIndicator rightMoveTransform;
    [SerializeField] private MoveIndicator wrongMoveTransform;
    [SerializeField] private TouchDetector touchDetector;

    private bool _isActive;
    private Animator _animator;

    private int _possibleDamage;

    private void Awake()
    {
        _animator = bigTargetTransform.GetComponent<Animator>();
    }


    public void Update()
    {
        
        if (!_isActive) return;
        
        if (touchDetector.TouchInZone())
        {
            CheckForInTarget();
        }
        
        UpdateDamageText();
    }

    private void UpdateDamageText()
    {
        if (!_isActive) return;
        
        if (bigTargetTransform.localScale.x > minTargetPoint.x 
                && bigTargetTransform.localScale.x < maxTargetPoint.x)
        {
            _possibleDamage = Random.Range(0, 100);
            damageText.text = _possibleDamage.ToString();
        }
        else
        {
            damageText.text = "0";
        }
    }

    private void CheckForInTarget()
    {
        if (bigTargetTransform.localScale.x > minTargetPoint.x 
            && bigTargetTransform.localScale.x < maxTargetPoint.x)
        {
            rightMoveTransform.Activate();
            GameController.instance.playerCharacter.animator.SetTrigger("attack");
            damageText.text = _possibleDamage.ToString();
            EndMiniGame();
            GameController.instance.playerCharacter.StartAttack();
            StartCoroutine(MoveWithDelay());
        }
        else
        {
            wrongMoveTransform.Activate();
            GameController.instance.enemyCharacter.animator.SetTrigger(AnimationHelper.SMALL_DAMAGE_TRIGGER);
            StartCoroutine(MoveWithDelay());
        }
    }

    private IEnumerator MoveWithDelay()
    {
        EndMiniGame();
        yield return new WaitForSeconds(0.7f);
    }

    public void StartMiniGame()
    {
        _animator.enabled = true;
        bigTargetTransform.gameObject.SetActive(true);
        smallTargetTransform.gameObject.SetActive(true);
        _isActive = true;
    }

    public void EndMiniGame()
    {
        _isActive = false;
        _animator.enabled = false;
        StartCoroutine(HideMiniGameWithDelay());
    }

    private IEnumerator HideMiniGameWithDelay()
    {
        Debug.Log("The game ends");
        yield return  new WaitForSeconds(0.5f);
        bigTargetTransform.gameObject.SetActive(false);
        smallTargetTransform.gameObject.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("GotIt");
    }
}
