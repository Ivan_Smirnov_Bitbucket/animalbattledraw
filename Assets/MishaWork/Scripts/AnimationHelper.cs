﻿using System.Collections;
using System.Threading;
using UnityEngine;

namespace AnimalBattle
{
    public static class AnimationHelper
    {
        public const string MISS_TRIGGER = "miss";
        public const string ACTIVATE_TRIGGER = "activate";
        public const string ATTACK_TRIGGER = "attack";
        public const string IS_DEFENDING_BOOL = "isDefending";
        public const string SMALL_DAMAGE_TRIGGER = "takeSmallDamage";
        public const string BIG_DAMAGE_TRIGGER = "takeBigDamage";
        public const string DIE_TRIGGER = "die";
        public const string IS_CHANGING_BOOL = "isChanging";
        public const string RESURRECT_TRIGGER = "resurrect";
        public const string VICTORY_TRIGGER = "victory";
        public const string IS_PLAYER_ZOOMING_BOOL = "cameraIsPlayerZooming";
        public const string IS_ENEMY_ZOOMING_BOOL = "cameraIsEnemyZooming";

        public static IEnumerator PlayAnimationWithDelay(Animator animator, 
            string animationTag, float delay)
        {
            yield return new WaitForSeconds(delay);
            animator.SetTrigger(animationTag);
        }
    }
}