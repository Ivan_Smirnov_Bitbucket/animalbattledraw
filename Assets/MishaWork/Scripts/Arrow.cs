﻿using System;
using System.Collections;
using System.Collections.Generic;
using AnimalBattle;
using GestureRecognizer;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

 
    public class Arrow : MonoBehaviour
    {
        public float leftSpeed = 140;

        public Image gestureImage;
        public GesturePattern gesturePattern;
        
        public bool isLast;

        public Animator animator;
        private bool _arrowMissed;

        // public Image image;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            gestureImage = GetComponent<Image>();
        }
        
        public void ArrowCompleted ()
        {
            if (GameController.instance.CurrentState == GameState.SceneReloading) return;

            if (isLast)
            {
                GameController.instance.playerCharacter.damage =
                    GameController.instance.enemyCharacter.health.healthPoints;
                GameController.instance.playerCharacter.damageIsSet = true;
            }

            animator.SetTrigger(AnimationHelper.ACTIVATE_TRIGGER);
            GameController.instance.playerCharacter.comboPoints++;
        }
        public void ArrowMissed ()
        {
            if (GameController.instance.CurrentState == GameState.SceneReloading) return;

            if (isLast)
            {
                GameController.instance.enemyCharacter.damage =
                    GameController.instance.playerCharacter.health.healthPoints;
                GameController.instance.enemyCharacter.damageIsSet = true;
            }

            GameController.instance.TurnOffParticles();

            animator.SetTrigger(AnimationHelper.MISS_TRIGGER);
            GameController.instance.EnemyAttack();
            GameController.instance.playerCharacter.comboPoints = 0;
        }

        //animation event
        public void DestroyArrow()
        {
            FindObjectOfType<GesturesSpawner>().gesturesList.Remove(this);
            Destroy(this.gameObject);
        }

        private void Update()
        {
            transform.position += Vector3.left * (leftSpeed * Time.deltaTime);
        }

        // public UnityEvent readyToSpawnNext = new UnityEvent ();
    }
