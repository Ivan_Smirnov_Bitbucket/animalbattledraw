﻿using System;
using System.Collections;
using System.Collections.Generic;
using GestureRecognizer;
using UnityEngine;
using UnityEngine.UI;

public class GestureLineDuplicator : MonoBehaviour
{
    [SerializeField] private GameObject particleSystem;

    public void DuplicateLine()
    {
        GameObject instantiatedParticles =
            Instantiate(particleSystem, transform);
        
        particleSystem.gameObject.SetActive(false);
        ParticleSystem particleSystemToOperate = instantiatedParticles.GetComponent<ParticleSystem>();

        var mainModule = particleSystemToOperate.main;
        
        mainModule.startLifetime = 10.0f;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            DuplicateLine();
        }
    }
}
