﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRender : MonoBehaviour
{
    private Camera _camera;
    private Texture _texture;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _texture = _camera.targetTexture;

        _texture.height = Screen.height;
        _texture.width = Screen.width;
    }
}
