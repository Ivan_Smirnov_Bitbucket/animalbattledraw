﻿using System.Net.Mime;
using UnityEngine;

namespace AnimalBattle
{
    [CreateAssetMenu(fileName = "Character Data", menuName = "Character Data", order = 0)]
    public class CharacterData : ScriptableObject
    {
        public Entity characterPrefab;
        public Sprite characterIcon;
    }
}