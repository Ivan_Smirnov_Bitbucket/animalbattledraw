﻿using System;
using UnityEngine;
using System.Collections;

namespace AnimalBattle
{
    public class PlayerCharacter : Entity
    {
        [SerializeField] private MiniGameController miniGameController;

        public bool canAttack = false;

        private Enemy _enemyCharacter;
        public int comboPoints;

        private void Start()
        {
            _enemyCharacter = (Enemy)GameController.instance.enemyCharacter;
            health.onDieEvent += Health_OnDieEvent;
        }

        private void Health_OnDieEvent(object sender, EventArgs e)
        {
            rotator.Rotate(0, 10);
            animator.SetTrigger(AnimationHelper.DIE_TRIGGER);
            GameController.instance.enemyCharacter.animator.SetTrigger(AnimationHelper.VICTORY_TRIGGER);
            GameController.instance.enemyCharacter.rotator.Rotate(70, 10);
            GameController.instance.mainCamera.GetComponent<Animator>().
                SetBool(AnimationHelper.IS_ENEMY_ZOOMING_BOOL, true);
            GameController.instance.CurrentState = GameState.SceneReloading;
            GameController.instance.NextLevel();
            //GameController.instance.EndGame();
        }

        public override void Attack()
        {
            GameController.instance.enemyCharacter.health.ApplyDamage(damage);
            animator.SetBool(AnimationHelper.IS_DEFENDING_BOOL, true);
        }
    }
}