﻿using UnityEngine;

namespace AnimalBattle
{
    public class AboveCharacterFigureSpawner : MonoBehaviour
    {
        [SerializeField] private GestureFigure gestureFigurePrefab;
        [SerializeField] private Transform spawnPoint;

        public void SpawnFigure(Sprite figureSprite)
        {
            GestureFigure spawnedFigure = Instantiate(gestureFigurePrefab, spawnPoint.transform);
            spawnedFigure.GetComponent<SpriteRenderer>().sprite = figureSprite;
            
            Debug.Log(spawnedFigure.gameObject.name + " is spawned!");
        }
        
    }
}