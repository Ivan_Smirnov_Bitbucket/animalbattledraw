﻿using UnityEngine;

namespace AnimalBattle
{
    public class AuraParticlesPresenter : MonoBehaviour
    {
        public ParticleSystem basePulseParticles;
        public ParticleSystem baseParticles;
        public ParticleSystem lineParticles;
        public ParticleSystem starsParticles;
    }
}