﻿using UnityEngine;

namespace AnimalBattle
{
    public class CharacterPair : MonoBehaviour
    {
        public CharacterData playerCharacter;
        public CharacterData enemyCharacter;
    }
}