﻿using UnityEngine;
using TMPro;

namespace AnimalBattle
{
    public class TextPopup : MonoBehaviour
    {
        private float dissapearTimer;
        private string text;
        private float alphaChangeDelta;

        private TextMeshPro popupText;
        private Color textColor;

        public static void Create (Vector3 createPosition, float lifeTime, string text)
        {
            Transform instText = Instantiate (ObjectsDatabase.instance.textPopupPrefab,
                createPosition, Quaternion.Euler(new Vector3(0,
                    230, 0)));

            instText.GetComponent<TextPopup>().SetUp (lifeTime, text);
        }

        private void SetUp (float lifeTime, string text)
        {
            this.dissapearTimer = lifeTime;
            this.text = text;
            GetComponent<TextMeshPro> ().text = text;
        }

        private void Awake ()
        {
            popupText = GetComponent<TextMeshPro> ();
        }

        private void Update ()
        {
            transform.position += new Vector3(0f, 0.5f) * Time.deltaTime;
            dissapearTimer -= Time.deltaTime;
            if (dissapearTimer <= 0)
            {
                float dissapearSpeed = 3.0f;
                textColor.a -= dissapearSpeed * Time.deltaTime;
                popupText.color = textColor;
                if (popupText.color.a <= 0)
                {
                    Destroy (gameObject);
                }
            }
        }
    }
}