﻿using System.Collections.Generic;
using AnimalBattle;
using GestureRecognizer;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

    public class Target : MonoBehaviour 
    {
        [SerializeField] private TextMeshProUGUI textResult;
        [SerializeField] private AboveCharacterFigureSpawner figureSpawner;
        public Recognizer recognizer;

        public List<Arrow> triggeredArrows = new List<Arrow> ();

        public void Clear () 
        {
            triggeredArrows.Clear ();
            recognizer.patterns.Clear ();
        }

        private void OnTriggerEnter2D (Collider2D other) 
        {
            var arrow = other.GetComponent<Arrow> ();
            triggeredArrows.Add (arrow);
            recognizer.patterns.Add (arrow.gesturePattern);
        }
        private void OnTriggerExit2D (Collider2D other) 
        {
            var arrow = other.GetComponent<Arrow> ();

            if (triggeredArrows.Contains (arrow)) {
                triggeredArrows.Remove (arrow);
                recognizer.patterns.Remove (arrow.gesturePattern);
                arrow.ArrowMissed ();
            }
        }
        public void OnRecognize (RecognitionResult result) {
            if (GameController.instance.CurrentState == GameState.Playing) {
                if (result != RecognitionResult.Empty)
                {
                    int scoreResult = Mathf.RoundToInt(result.score.score * 100);
                    //textResult.text = result.gesture.id + "\n" + scoreResult + "%";

                    if (triggeredArrows.Count > 0) {

                        var index = triggeredArrows.FindIndex (x => result.gesture == x.gesturePattern);
                        if (index >= 0) {

                            for (int i = 0; i < index; i++) {
                                triggeredArrows[i].ArrowMissed ();
                                recognizer.patterns.Remove (triggeredArrows[i].gesturePattern);
                                triggeredArrows.Remove (triggeredArrows[i]);
                            }

                            triggeredArrows[0].ArrowCompleted ();
                            CheckForComboPoints();
                            GameController.instance.PlayerAttack(scoreResult);
                            
                            //CalculateScore (triggeredArrows[0]);
                            recognizer.patterns.Remove (triggeredArrows[0].gesturePattern);
                            triggeredArrows.Remove (triggeredArrows[0]);
                        }
                    }
                }
                else 
                {
                    if (triggeredArrows.Count != 0)
                        triggeredArrows[0].GetComponent<Animator>().SetTrigger(
                            AnimationHelper.MISS_TRIGGER);
                    
                    GameController.instance.figureWinParticles.Stop();
                }
            }
        }

        private void CheckForComboPoints()
        {
            /*
            int comboPoints = GameController.instance.playerCharacter.comboPoints;
            
            if (comboPoints >= 3)
            {
                if (comboPoints == 3)
                {
                    GameController.instance.auraParticlesPresenter.basePulseParticles.Play();
                        GameController.instance.auraParticlesPresenter.baseParticles.Play();
                }
                if (comboPoints > 3)
                {
                    GameController.instance.auraParticlesPresenter.lineParticles.Play();
                }

                if (comboPoints > 4)
                {
                    GameController.instance.auraParticlesPresenter.starsParticles.Play();
                }
                */
                GameController.instance.figureWinParticles.Play();

                figureSpawner.SpawnFigure(triggeredArrows[0].gestureImage.sprite);
        }

        public void CalculateScore (Arrow arrow) 
        {
            var rect = GetComponent<RectTransform> ();

            var distance = (rect.position.x - arrow.GetComponent<RectTransform> ().position.x) / rect.lossyScale.x;

            /*
            if (Mathf.Abs (distance) < rect.rect.width / 15) {
                banner.Configure (Banner.Category.Legendary, "Невероятно!", "Идеально!", "Легендарно!");
                GameManager.Instance.Score += 200;

                Debug.Log ("Legendary " + Mathf.Abs (distance));

            } else if (Mathf.Abs (distance) < rect.rect.width / 7) {
                banner.Configure (Banner.Category.Perfect, "Отлично!", "Супер!", "Круто!");
                GameManager.Instance.Score += 100;

                Debug.Log ("Perfect " + Mathf.Abs (distance));

            } else if (Mathf.Abs (distance) < rect.rect.width / 4) {
                banner.Configure (Banner.Category.Good, "Хорошо!", "Неплохо!", "Продолжай!");
                GameManager.Instance.Score += 50;

                Debug.Log ("Good " + Mathf.Abs (distance));

            } else if (distance < 0) {
                banner.Configure (Banner.Category.Neutral, "Рановато!", "Слишком рано!");
                GameManager.Instance.Score += 25;

                Debug.Log ("Neutral " + distance);

            } else {
                banner.Configure (Banner.Category.Neutral, "В последний момент!", "Успел!", "На грани!");
                GameManager.Instance.Score += 25;

                Debug.Log ("Neutral " + distance);

            }
            */
        }
    }